require 'bundler'
Bundler.require

$LOAD_PATH << File.expand_path('../', __FILE__)
$LOAD_PATH << File.expand_path('../lib', __FILE__)

require 'app/models'
require 'app/routes'

module Brisk
  class App < Sinatra::Application
    configure do
      disable :method_override
      disable :static
      # set :bind => '0.0.0.0'
      # set :environment => :production
      # set :port, 9000
    end
    use Rack::Deflater

    use Brisk::Routes::Index
  end
end
