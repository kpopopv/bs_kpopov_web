require 'lib/board'
require 'lib/ship_builder'
require 'lib/strategy'
require 'lib/opponent'

module Brisk
  module Models
    autoload :Positions, 'app/models/positions'
  end
end
