require 'csv'
module Brisk
  module Models
    class Positions
      PLAYER_TYPE = {
        player: 0,
        computer: 1
      }

      SHIP_SEPARATOR = 'end of ship definition'

      DB_PATH = Sinatra::Application.settings.root + '/app/db'

      class << self
        def prepare
          PLAYER_TYPE.keys.each do |key|
            File.delete(get_db_path(key)) if File.exist?(get_db_path(key))
          end
        end

        def save(positions, player_type)
          File.open(get_db_path(PLAYER_TYPE.keys[player_type]), 'a+') do |f|
            positions.each do |item|
              if item.first.class == Array
                f.puts [item.first[0], item.first[1], item[1]].join(',')
              else
                f.puts [item[0], item[1], 0.to_s].join(',')
              end
            end
            f.puts SHIP_SEPARATOR
          end
          true
        end

        def save_ships(ships, player_type)
          File.delete(get_db_path(PLAYER_TYPE.keys[player_type])) if File.exist?(get_db_path(PLAYER_TYPE.keys[player_type]))
          ships.each { |ship| save(ship, player_type) }
        end

        def load(player_type)
          positions = []
          ship = []
          path = get_db_path(PLAYER_TYPE.keys[player_type])
          return false unless File.exist?(path)
          CSV.read(path).each do |row|
            if row[0] == SHIP_SEPARATOR
              positions << ship
              ship = []
            else
              ship << [[row[0], row[1]].map(&:to_i), row[2].to_i]
            end
          end
          positions
        end

        def ranking_valid?(ship_validation_counter, key)
          load(PLAYER_TYPE[key]).each { |ship| ship_validation_counter[ship.size] -= 1 } if load(PLAYER_TYPE[key])
          ship_validation_counter.values.uniq.count == 1 && ship_validation_counter.values.first == 0
        end

        def update(positions, target)
          positions.map! do |ship|
            ship.map! do |coord, status|
              (coord == target && status == 0) ? [coord, 1] : [coord, status]
            end
          end
          positions
        end

        def check_hit(positions, target)
          positions.each do |ship|
            ship.each do |coord, status|
              return true if coord == target && status == 0
            end
          end
          false
        end

        def game_over?(positions)
          positions.flatten(1).map { |cell| cell[1] }.inject(:+) == 20
        end

        private

        def get_db_path(key)
          File.join(DB_PATH, key.to_s + '_positions.csv')
        end
      end
    end
  end
end
