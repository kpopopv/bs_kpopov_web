require 'json'
module Brisk
  module Routes
    class Index < Sinatra::Application
      include Brisk::Models
      include Brisk::Libraries

      configure do
        set :views, 'app/views'
        set :root, File.expand_path('../../../', __FILE__)

        disable :method_override
        set :show_exceptions, :after_handler
      end

      get '/' do
        erb :index
      end

      post '/prepare' do
        content_type :json
        Positions.prepare
        Opponent.prepare
        { status: 1 }.to_json
      end

      post '/place' do
        content_type :json

        # get positions from file
        existing_positions = Positions.load(Positions::PLAYER_TYPE[:player])
        recent_positions = params['positions'].values.map { |item| item.map(&:to_i) }
        board = Board.new

        # if positions can be placed successfully
        if (existing_positions && board.update_possible_choices(existing_positions)) || !existing_positions
          if board.check_suitable(recent_positions)
            # add positions to file
            Positions.save(recent_positions, Positions::PLAYER_TYPE[:player])
            { status: 1 }.to_json
          else
            halt(422, 'Ship can not be added')
          end
        else
          halt(422, 'Ship can not be added')
        end
      end

      post '/start' do
        content_type :json
        # check ships ranking
        if Positions.ranking_valid?(Board::SHIPS.clone, :player)

          Positions.save_ships(Board.new.get_ships, Positions::PLAYER_TYPE[:computer])
          { status: 1 }.to_json
        else
          halt(422, 'Fail, Invalid ranking')
        end
      end

      post '/shoot' do
        content_type :json

        position = params['position'].map(&:to_i)

        result = {
          status: 0,
          cell: false,
          opp_shoot: []
        }

        # check if ranking correctly
        Positions::PLAYER_TYPE.each_key do |key|
          halt(422, 'Fail, Invalid ranking') unless Positions.ranking_valid?(Board::SHIPS.clone, key)
        end

        player_positions = Positions.load(Positions::PLAYER_TYPE[:player])
        computer_positions = Positions.load(Positions::PLAYER_TYPE[:computer])

        # check if player hit
        if Positions.check_hit(computer_positions, position)
          computer_positions = Positions.update(computer_positions, position)
          Positions.save_ships(computer_positions, Positions::PLAYER_TYPE[:computer])
          result[:cell] = true
          # player win if player has not life cells already
          result[:status] = 1 if Positions.game_over?(computer_positions)
        end

        # make shoot by computer if he is not lose
        unless result[:status] == 1
          opponent = Opponent.new(player_positions)
          result[:opp_shoot] = opponent.shoot

          if result[:opp_shoot][1] == 1
            player_positions = Positions.update(player_positions, result[:opp_shoot].first)
            Positions.save_ships(player_positions, Positions::PLAYER_TYPE[:player])
          end
          # computer win if player has not life cells already
          result[:status] = 2 if Positions.game_over?(player_positions)
        end
        result.to_json
      end
    end
  end
end
