module Brisk
  module Libraries
    class Board
      SHIPS = { 4 => 1, 3 => 2, 2 => 3, 1 => 4 }

      def self.clean_outer(coords)
        coords.keep_if { |x, y| (x >= 0) && (x <= 9) && (y >= 0) && (y <= 9) }
      end

      def initialize
        @possible_choices = Array.new(10) do |x|
          Array.new(10) do |y|
            [x, y]
          end
        end.flatten(1)
      end

      def update_possible_choices(positions)
        positions.each do |ship|
          surrounding_cells = []
          ship.each { |item| surrounding_cells += cell_surrounding(item.first) }
          @possible_choices.delete_if { |coords| (surrounding_cells.uniq + ship.first).include? coords }
        end
      end

      def check_suitable(ship)
        # if validation needed
        if ship.size > 1
          # strategy validation
          2.upto(ship.size) do |number|
            return false unless Strategy.new(ship[0..number - 2], ship.size).places.include? ship[number - 1]
          end
        end
        # possible_choices validation
        ship.each { |coord| return false unless @possible_choices.include? coord }
        true
      end

      def get_ships
        ships = []
        SHIPS.each do |type, count|
          1.upto(count) do
            loop do
              next unless ship = ShipBuilder.new(type, @possible_choices).try_to_place
              update_possible_choices([ship.map! { |coord| [coord, 0] }])
              ships << ship
              break
            end
          end
        end
        ships
      end

      private

      def cell_surrounding(coord)
        coords = []
        [[0, 1], [0, -1], [-1, -1], [-1, 0], [-1, 1], [1, -1], [1, 0], [1, 1]].each do |shift|
          coords << [coord[0] + shift[0], coord[1] + shift[1]]
        end
        coords
      end
    end
  end
end
