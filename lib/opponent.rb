module Brisk
  module Libraries
    class Opponent
      STORE_PATH = Sinatra::Application.settings.root + '/var/opponent.json'

      def initialize(player_positions)
        @player_positions = player_positions

        unless load
          @positions = []
          @computed_options = []
          @possible_choices = Array.new(10) do |x|
            Array.new(10) do |y|
              [x, y]
            end
          end.flatten(1)
        end
      end

      def self.prepare
        File.delete(STORE_PATH) if File.exist?(STORE_PATH)
      end

      def shoot
        coordinate = get_available_options.sample
        is_hit = @player_positions.flatten(1).detect { |coord| coord.first == coordinate }
        # if shoot successful
        if is_hit
          if @positions.size == 3
            @computed_options = []
          else
            @positions << coordinate
            @computed_options = Board.clean_outer(Strategy.new(@positions, @positions.size + 1).places)
          end
        end
        update_computed_options(coordinate)
        [[coordinate[0], coordinate[1]], is_hit ? 1 : 0]
      end

      private

      def update_computed_options(coordinate)
        @computed_options.delete(coordinate)
        @possible_choices.delete(coordinate)
        unless get_computed_options.any?
          @positions = []
          @computed_options = []
        end
        save
      end

      def get_available_options
        get_computed_options.any? ? get_computed_options : @possible_choices
      end

      def get_computed_options
        @possible_choices & @computed_options
      end

      def load
        return false unless File.exist?(STORE_PATH)
        data = File.open(STORE_PATH, 'rb').read
        if data.length >= 2
          data = JSON.parse(File.open(STORE_PATH, 'rb').read)
          @positions, @computed_options, @possible_choices = data.values
          true
        end
      end

      def save
        File.open(STORE_PATH, 'w') { |file| file.write(to_json) }
      end

      def to_json
        {
          positions: @positions,
          computed_options: @computed_options,
          possible_choices: @possible_choices
        }.to_json
      end
    end
  end
end
