module Brisk
  module Libraries
    class ShipBuilder
      def initialize(deck_size, possible_choices)
        @deck_size = deck_size
        @possible_choices = possible_choices
        @coords = []
      end

      def try_to_place
        try_to_place_recursive(@deck_size, @possible_choices)
      end

      private

      def try_to_place_recursive(deck_remaining, possible_choices)
        if deck_remaining > 0
          return false unless push_cell(possible_choices)
          try_to_place_recursive(deck_remaining -= 1, possible_choices)
        else
          @coords
        end
      end

      def push_cell(possible_choices)
        if @coords.size == 0
          @coords << possible_choices.sample
        else
          available_places = Board.clean_outer(Strategy.new(@coords, @deck_size).places) & possible_choices
          return false unless available_places.any?
          @coords << available_places.sample
        end
        true
      end
    end
  end
end
