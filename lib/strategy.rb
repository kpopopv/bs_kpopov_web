module Brisk
  module Libraries
    class Strategy
      def initialize(positions, decks)
        @positions = positions
        @decks = decks
      end

      def places
        available_coords = []

        # Returns palaces that can be used for placing next ship cell.
        if (@decks == 3 || @decks == 4) && (@positions.size >= 2)
          x_coordinates = get_pair (@positions) { |coords| coords.map { |i| i[0] } }
          y_coordinates = get_pair (@positions) { |coords| coords.map { |i| i[1] } }
          # Если горизонтальная ориентация
          if x_coordinates[0] == x_coordinates[1]
            available_coords = [[x_coordinates.first, y_coordinates[0] + 1], [x_coordinates.first, y_coordinates[1] - 1]]
          else
            available_coords = [[x_coordinates[0] + 1, y_coordinates.first], [x_coordinates[1] - 1, y_coordinates.first]]
          end
        end
        # Клетка должна соприкосаться с предыдущей, то есть выполняется одно из условий, если одна из координат первой клетки равна второй, то смотрим, чтобы вторая координата располагалась рядом
        if @positions.size == 1 && @decks > 1
          available_coords = [[@positions[0][0], @positions[0][1] - 1],
                              [@positions[0][0], @positions[0][1] + 1],
                              [@positions[0][0] - 1, @positions[0][1]],
                              [@positions[0][0] + 1, @positions[0][1]]]
        end

        # allow to set specify variations for 3 and 4 cells of 4 deck ship
        if @decks == 4 && @positions.size >= 2
          x_coordinates = get_pair (@positions) { |coords| coords.map { |i| i[0] } }
          y_coordinates = get_pair (@positions) { |coords| coords.map { |i| i[1] } }
          # if horizontal orientation
          if x_coordinates[0] == x_coordinates[1]
            y_coordinates[1].upto(y_coordinates[0]) do |i|
              available_coords << [x_coordinates.first - 1, i]
              available_coords << [x_coordinates.first + 1, i]
            end
          elsif y_coordinates[0] == y_coordinates[1]
            x_coordinates[1].upto(x_coordinates[0]) do |i|
              available_coords << [i, y_coordinates.first - 1]
              available_coords << [i, y_coordinates.first + 1]
            end
            # special 4 cell for not smooth 4 deck ship with 3 cells
          elsif @positions.size == 3
            available_coords = get_specify_fourth_cell(@positions)
          end
        end
        available_coords
      end

      def get_specify_fourth_cell(ship)
        direction = []
        # Center contact with the cells 2
        center = ship.detect do |coord|
          direction = []
          shift = {
            'left' => [coord[0], coord[1] + 1],
            'right' => [coord[0], coord[1] - 1],
            'top' => [coord[0] + 1, coord[1]],
            'bottom' => [coord[0] - 1, coord[1]]
          }
          shift.each { |index, value| direction << index if ship.include? value }
          direction.size == 2
        end

        shift_for_coords = {
          'right_bottom' => [[0, -2], [-2, 0], [0, 1], [1, 0]],
          'right_top' => [[0, -2], [2, 0], [0, 1], [-1, 0]],
          'left_bottom' => [[-2, 0], [0, 2], [0, -1], [1, 0]],
          'left_top' => [[0, 2], [2, 0], [-1, 0], [0, -1]]
        }

        target_coords = []
        shift_for_coords[direction.join('_')].each do |x, y|
          target_coords << [center[0] + x, center[1] + y]
        end
        target_coords
      end

      private

      def get_pair(list)
        coordinates = yield list
        [coordinates.max, coordinates.min]
      end
    end
  end
end
