(($) ->
  class sb
    constructor: ->
      @player_cell = '.map.player .rectangle'
      @cell = '.map .rectangle'
      @ship_type
      @latest_event
      @positions = []

      @init()
      @set_mode 'ranking'
      @set_possible_choices()

      $('.ship-type').click (e) =>
        return false if $(e.currentTarget).hasClass 'disabled'
        @positions = []
        unless $(e.target).hasClass 'active'
          @ship_type = parseInt $(e.currentTarget).data('type')
          $(e.currentTarget).addClass('active')
          .siblings()
          .removeClass 'active'

          $(@player_cell).hover ((event) =>
            @latest_event = event.type
            coordinate = @parse_coords event.target

            if include(coordinate, @possible_choices)
              $(event.target).addClass 'active'
              # if validation needed
              if @positions.length
                unless @check_coordinate(coordinate)
                  $(event.target).addClass 'unavailable'
                  return
            else
              $(event.target).addClass 'unavailable'
              return
          ), ->
            $(event.target).removeClass 'active unavailable'
            return

          $(@player_cell).click (event) =>
            @latest_event = event.type
            coordinate = @parse_coords(event.target)
            # if strategy validation not passed
            return false if @positions.length and not @check_coordinate(coordinate)
            # if possible choices not contain coordinate
            return false unless include(coordinate, @possible_choices)

            if coordinate
              @set_deck event.target
              @positions.push coordinate
              @allowed_positions = @get_places()
              # if ship established
              if @positions.length == @ship_type
# it's must be here
                $(event.target).addClass('filled')
                .removeClass 'active'
                @update_possible_choices()
                @update_counter()
                @unselect_ship_type()
                return
        else
          @unselect_ship_type()

        $(window).keydown (event) =>
# can't revert if cell already installed
          if event.which == 27 and not (@latest_event == 'click' and not @ship_type)
            for i of @positions
              $("#map-1-#{@positions[i][0]}-#{@positions[i][1]}").removeClass 'filled'
            $(@cell).removeClass 'active unavailable'
            @unselect_ship_type()
            @allowed_positions = []
            @positions = []
          return
        return

      @ranking_finished = =>
        opponent_ranking =
          [
            [0, 0], [0, 1], [0, 2], [0, 3],
            [0, 5], [0, 6], [0, 7], [2, 0],
            [2, 1], [2, 2], [2, 4], [2, 5],
            [2, 7], [2, 8], [4, 0], [4, 1],
            [4, 3], [4, 5], [4, 7]
          ]
        for i of opponent_ranking
          $("#map-0-#{opponent_ranking[i][0]}-#{opponent_ranking[i][1]}").addClass 'filled'
        $(@cell).click ->
          $(this).addClass 'has_shoot'
          return

  sb.prototype =
    init: ->
      $.each $('.map'), (index) ->
        for i in [0...10]
          for j in [0...10]
            $(this).append $('<div>',
              class: 'rectangle'
              id: 'map-' + index + '-' + i + '-' + j)
          $(this).append $('<div>', class: 'clearfix')
        return
      return
    set_mode: (mode) ->
      switch mode
        when 'ranking'
          $('.map.player').addClass 'ranking'
        when 'playing'
          $('.map.player').removeClass 'ranking'
          $('.map').addClass 'playing'
          @type_msg 'info', title: 'Игра началась!'
      return
    parse_coords: (element) ->
      cell_attributes = element.id.split('-')
      [
        parseInt(cell_attributes[2])
        parseInt(cell_attributes[3])
      ]
    get_places: ->
      new strategy(@positions, @ship_type).places()
    update_counter: ->
      counter = $('.ship-type' + '[data-type=\'' + @ship_type + '\'] .counter')
      count = parseInt(counter.text()) - 1
      if count == 0
        $(counter).parent().addClass 'disabled'
      counter.html count
      # is ranking finished
      if $('.ship-type.disabled').length == 1
        @set_mode 'playing'
        @ranking_finished()
      return
    unselect_ship_type: ->
      @ship_type = false
      $('.ship-type').removeClass 'active'
      $(@player_cell).unbind 'mouseenter mouseleave click'
      return
    check_coordinate: (coordinate) ->
      if include(coordinate, @allowed_positions)
        return coordinate
      false
    set_deck: (element) ->
      $(element).mouseleave ->
        $(this).addClass 'filled'
        return
      return
    set_possible_choices: ->
      @possible_choices = []
      for i in [0...10]
        for j in [0...10]
          @possible_choices.push [i, j]
      return
    update_possible_choices: ->
      terminated_cells = @get_terminated_cells()
      for i of terminated_cells
        @possible_choices = delete_if(terminated_cells[i], @possible_choices)
      return
    get_terminated_cells: ->
      coords = []
      shifts = [
        [0, 1]
        [0, -1]
        [-1, -1]
        [-1, 0]
        [-1, 1]
        [1, -1]
        [1, 0]
        [1, 1]];
      for j of @positions
        coords.push @positions[j]
        for i of shifts
          coords.push [
            @positions[j][0] + shifts[i][0]
            @positions[j][1] + shifts[i][1]
          ]
      unique (new strategy).clean_other(coords)
    type_msg: (status, options) ->
      content = undefined
      if options.title?
        content = $('<h4>', text: options.title)
      if options.content?
        content += $('<p>', text: options.content)
      $('#messages').append $('<div>',
        class: status
        html: content)
      return
  new sb
  return) jQuery