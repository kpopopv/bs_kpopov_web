(function ($) {
    var sb = function () {

        this.player_cell = '.map.player .rectangle', this.opponent_cell = '.map.opponent .rectangle', this.cell = '.map .rectangle';
        this.ship_type, this.latest_event = null;
        this.positions = [];
        this.waiting_opponent = false;

        var that = this;
        this.init();
        this.prepare_game();
        this.set_mode('ranking');
        this.set_possible_choices();

        $('.ship-type').click(function () {
            // if ships with certain deck count already unavailable
            if ($(this).hasClass('disabled')) {
                return false;
            }

            that.positions = [];
            // if this ships type already selected
            if (!$(this).hasClass('active')) {
                that.ship_type = parseInt($(this).data('type'));

                $(this).addClass('active')
                    .siblings()
                    .removeClass('active');

                $(that.player_cell).hover(function (e) {
                    that.latest_event = e.type;
                    var coordinate = that.parse_coords(this);
                    if (include(coordinate, that.possible_choices)) {
                        $(this).addClass('active');
                        // if validation needed
                        if (that.positions.length) {
                            if (!that.check_coordinate(coordinate)) {
                                $(this).addClass('unavailable');
                            }
                        }
                    } else {
                        $(this).addClass('unavailable');
                    }
                }, function () {
                    $(this).removeClass('active unavailable');
                });

                $(that.player_cell).click(function (e) {
                    that.latest_event = e.type;

                    var coordinate = that.parse_coords(this);

                    // if strategy validation not passed
                    if (that.positions.length && !that.check_coordinate(coordinate)) {
                        return false;
                    }

                    // if possible choices not contain coordinate
                    if (!include(coordinate, that.possible_choices)) {
                        return false;
                    }

                    if (coordinate) {
                        that.set_deck(this);
                        that.positions.push(coordinate);
                        that.allowed_positions = that.get_places();
                        // if ship established
                        if (that.positions.length == that.ship_type) {
                            var currentCell = this;
                            $.ajax({
                                url: "/place",
                                data: {positions: that.positions},
                                method: 'POST'
                            }).error(function () {
                                that.revert_ship();
                                that.type_msg('danger', {title: 'Невозможно установить корабль!'});
                            }).success(function () {
                                // it's must be here because event can be overloaded if user will have not moved cursor
                                $(currentCell).addClass('filled');
                                that.update_possible_choices();
                                that.update_counter();
                            }).complete(function () {
                                that.unselect_ship_type();
                                $(that.cell).removeClass('active unavailable');
                            })
                        }
                    }
                });
            } else {
                that.unselect_ship_type();
            }

            $(window).keydown(function (eventObject) {
                // can't revert if cell already installed
                if (eventObject.which == 27 && !(that.latest_event == 'click' && !that.ship_type)) {
                    that.revert_ship();
                    that.unselect_ship_type();
                    $(that.cell).removeClass('active unavailable');
                }
            });
        });

        this.ranking_finished = function () {
            $.ajax({
                url: "/start",
                method: 'POST'
            }).error(function () {
                that.type_msg('danger', {title: 'Невозможно установить!'});
            }).success(function () {

                $(that.opponent_cell).click(function(){
                    if(that.waiting_opponent == true){
                        return false;
                    }
                    var coordinate = that.parse_coords(this);
                    var currentCell = this;
                    $.ajax({
                        url: "/shoot",
                        data: { position: coordinate },
                        method: 'POST'
                    }).error(function () {
                        that.type_msg('danger', {title: 'Некорректная расстановка!'});
                    }).success(function (data) {
                        if(data.status){
                            $(that.cell).unbind('mouseenter mouseleave click');
                            if(data.status == 1){
                                that.type_msg('info', {title: 'Поздравляем, вы победили!'});
                            }else if(data.status == 2){
                                that.type_msg('info', {title: 'В этот раз вам не повезло, попробуйте ещё раз!'});
                            }
                        }
                        // mark player shoot
                        $(currentCell).addClass('has_shoot');
                        // if player shoot is successful
                        if(data.cell){
                            that.play_sfx_bang();
                            $(currentCell).addClass('filled');
                        }

                        that.waiting_opponent = true;
                        // mark computer shoot
                        if(data.opp_shoot.length){
                            setTimeout(function(){
                                // if computer hit
                                if(data.opp_shoot[1]){
                                    that.play_sfx_bang();
                                }
                                $('#map-1-' + data.opp_shoot[0][0] + '-' +  data.opp_shoot[0][1]).addClass('has_shoot');
                                that.waiting_opponent = false;
                            }, 1000)

                        }
                    });
                })


            });
        }
    }

    sb.prototype = {
        init: function () {
            $.each($('.map'), function (index) {
                for (var i = 0; i < 10; i++) {
                    for (var j = 0; j < 10; j++) {
                        $(this).append($('<div>', {
                            class: 'rectangle',
                            id: 'map-' + index + '-' + i + '-' + j
                        }));
                    }
                    $(this).append($('<div>', {
                        class: 'clearfix',
                    }));
                }
            });

            $('#sfx-muted').click(function(){
                $(this).toggleClass('disabled');
                if ($(this).hasClass("disabled")) {
                    $('audio').prop("muted", true);
                }else{
                    $('audio').prop("muted", false);
                }
            })
        },
        prepare_game: function () {
            $.ajax({
                url: "/prepare",
                method: 'POST'
            }).done(function (data) {
                console.log('prepared status: ' + data.status);
            })
        },
        set_mode: function (mode) {
            switch (mode) {
                case 'ranking':
                    $('.map.player').addClass('ranking');
                    break;
                case 'playing':
                    $('.map.player').removeClass('ranking');
                    $('.map').addClass('playing');
                    this.type_msg('info', {title: 'Игра началась!'});
                    break;
            }
        },
        parse_coords: function (element) {
            var cell_attributes = element.id.split('-');
            return [parseInt(cell_attributes[2]), parseInt(cell_attributes[3])];
        },
        get_places: function () {
            return new strategy(this.positions, this.ship_type).places();
        },
        update_counter: function () {
            var counter = $('.ship-type' + "[data-type='" + this.ship_type + "'] .counter");
            var count = parseInt(counter.text()) - 1;
            if (count == 0) {
                $(counter).parent().addClass('disabled');
            }
            counter.html(count);

            // is ranking finished
            if ($('.ship-type.disabled').length == 4) {
                this.set_mode('playing');
                this.ranking_finished();
            }

        },
        unselect_ship_type: function () {
            this.ship_type = false;
            $('.ship-type').removeClass('active');
            $(this.player_cell).unbind('mouseenter mouseleave click')
        },
        check_coordinate: function (coordinate) {
            if (include(coordinate, this.allowed_positions)) {
                return coordinate;
            }
            return false;
        },
        set_deck: function (element) {
            $(element).mouseleave(function () {
                $(this).addClass('filled');
            });

        },
        set_possible_choices: function () {
            this.possible_choices = [];
            for (var i = 0; i < 10; i++) {
                for (var j = 0; j < 10; j++) {
                    this.possible_choices.push([i, j]);
                }
            }
        },
        update_possible_choices: function () {
            var terminated_cells = this.get_terminated_cells();
            for (var i in terminated_cells) {
                this.possible_choices = delete_if(terminated_cells[i], this.possible_choices);
            }
        },
        get_terminated_cells: function () {
            var coords = [];
            var shifts = [[0, 1], [0, -1], [-1, -1], [-1, 0], [-1, 1], [1, -1], [1, 0], [1, 1]];
            for (var j in this.positions) {
                coords.push(this.positions[j]);
                for (var i in shifts) {
                    coords.push([this.positions[j][0] + shifts[i][0], this.positions[j][1] + shifts[i][1]])
                }
            }
            return unique(new strategy().clean_other(coords));
        },
        type_msg: function (status, options) {
            var content;
            if (options.hasOwnProperty('title')) {
                content = $('<h4>', {text: options.title});
            }
            if (options.hasOwnProperty('content')) {
                content += $('<p>', {text: options.content});
            }
            $('#messages').prepend($('<div>', {
                class: status,
                html: content
            }));
        },
        revert_ship: function () {
            for (var i in this.positions) {
                $('#map-1-' + this.positions[i][0] + '-' + this.positions[i][1]).removeClass('filled');
            }
            this.allowed_positions = [];
            this.positions = [];
        },
        play_sfx_bang:function(){
            $('#sfx-bang').get(0).stop().play();
        }
    }

    new sb();

})(jQuery);
