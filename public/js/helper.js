HTMLAudioElement.prototype.stop = function()
{
    this.pause();
    this.currentTime = 0.0;
    return this;
}

function include(toMatch, array) {
    for (var i in array) {
        if (array[i].toString() == toMatch.toString()) {
            return true;
        }
    }
    return false;
}
function unique(arr) {
    var hash = {}, result = [];
    for (var i = 0, l = arr.length; i < l; ++i) {
        if (!hash.hasOwnProperty(arr[i])) { //it works with objects! in FF, at least
            hash[arr[i]] = true;
            result.push(arr[i]);
        }
    }
    return result;
}
function delete_if(value, array) {
    return array.filter(function (e) {
        return e.toString() !== value.toString()
    });
}

