var strategy = function (positions, decks) {
    this.positions = positions;
    this.decks = decks;
}


strategy.prototype = {
    places: function () {

        var available_coordinates = this.placesDefault();
        // allow to set specify variations for 3 and 4 cells of 4 deck ship
        if (this.decks == 4 && this.positions.length >= 2) {
            var all_x = [], all_y = [];
            for (var i in this.positions) {
                all_x.push(this.positions[i][0]);
                all_y.push(this.positions[i][1]);
            }

            var x_coordinates = [this.max(all_x), this.min(all_x)];
            var y_coordinates = [this.max(all_y), this.min(all_y)];
            //if horizontal orientation
            if (x_coordinates[0] == x_coordinates[1]) {
                for (var i = y_coordinates[1]; i <= y_coordinates[0]; i++) {
                    available_coordinates.push([x_coordinates[0] - 1, i]);
                    available_coordinates.push([x_coordinates[0] + 1, i]);
                }
            } else if (y_coordinates[0] == y_coordinates[1]) {
                for (var i = x_coordinates[1]; i <= x_coordinates[0]; i++) {
                    available_coordinates.push([i, y_coordinates[0] - 1]);
                    available_coordinates.push([i, y_coordinates[0] + 1]);
                }
            } else if (this.positions.length == 3) {
                available_coordinates = this.get_specify_fourth_cell(this.positions)
            }
        }
        return this.clean_other(available_coordinates);
    },

    get_specify_fourth_cell: function (positions) {
        var direction = [];
        // Center contact with the cells 2
        var center;
        for (var i in positions) {
            var coord = positions[i];
            direction = [];
            var shift = {
                'left': [coord[0], coord[1] + 1],
                'right': [coord[0], coord[1] - 1],
                'top': [coord[0] + 1, coord[1]],
                'bottom': [coord[0] - 1, coord[1]]
            }

            for (var j in shift) {
                if (include(shift[j], positions)) {
                    direction.push(j);
                }
            }
            if (direction.length == 2) {
                // ruby detect
                center = coord;
                break;
            }
        }

        var shift_for_coords = {
            'right_bottom': [[0, -2], [-2, 0], [0, 1], [1, 0]],
            'right_top': [[0, -2], [2, 0], [0, 1], [-1, 0]],
            'left_bottom': [[-2, 0], [0, 2], [0, -1], [1, 0]],
            'left_top': [[0, 2], [2, 0], [-1, 0], [0, -1]]
        }


        var target_coords = [];

        shift_for_coords = shift_for_coords[direction.join('_')];
        for (var i in shift_for_coords) {
            var x = shift_for_coords[i][0];
            var y = shift_for_coords[i][1];
            target_coords.push([center[0] + x, center[1] + y])
        }
        return target_coords;
    },

    clean_other: function (positions) {
        var filter = [];
        for (var i in positions) {
            var coord = positions[i];
            if (coord[0] >= 0 && coord[1] >= 0 && coord[0] <= 9 && coord[1] <= 9) {
                filter.push(coord);
            }
        }
        return filter;
    },
    placesDefault: function () {
        if ((this.decks == 3 || this.decks == 4) && (this.positions.length >= 2)) {
            var all_x = [], all_y = [], available_coordinates = [];
            for (var i in this.positions) {
                all_x.push(this.positions[i][0]);
                all_y.push(this.positions[i][1]);
            }

            var x_coordinates = [this.max(all_x), this.min(all_x)];
            var y_coordinates = [this.max(all_y), this.min(all_y)];

            //if horizontal orientation
            if (x_coordinates[0] == x_coordinates[1]) {
                available_coordinates = [[x_coordinates[0], y_coordinates[0] + 1], [x_coordinates[0], y_coordinates[1] - 1]]
            } else {
                available_coordinates = [[x_coordinates[0] + 1, y_coordinates[0]], [x_coordinates[1] - 1, y_coordinates[0]]]
            }
            return available_coordinates
        }

        // Клетка должна соприкосаться с предыдущей, то есть выполняется одно из условий, если одна из координат первой клетки равна второй, то смотрим, чтобы вторая координата располагалась рядом
        if (this.positions.length == 1 && this.decks > 1) {
            return [[this.positions[0][0], this.positions[0][1] - 1],
                [this.positions[0][0], this.positions[0][1] + 1],
                [this.positions[0][0] - 1, this.positions[0][1]],
                [this.positions[0][0] + 1, this.positions[0][1]]]
        }
    },
    max: function (array) {
        return Math.max.apply(Math, array);
    },
    min: function (array) {
        return Math.min.apply(Math, array);
    }
}

//var places = new strategy([[1, 2], [1, 3], [2, 3]], 4);
//console.log(places.places());


