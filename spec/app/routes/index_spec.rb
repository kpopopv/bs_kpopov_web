describe Index do
  describe "POST '/shoot'" do
    let(:computer_positions) do
      [
        [[[9, 1], 0], [[8, 1], 0], [[8, 0], 0], [[8, 2], 0]],
        [[[7, 4], 0], [[8, 4], 0], [[6, 4], 0]],
        [[[1, 2], 0], [[2, 2], 0], [[0, 2], 0]],
        [[[0, 6], 0], [[0, 7], 0]],
        [[[5, 1], 0], [[6, 1], 0]],
        [[[5, 9], 0], [[5, 8], 0]],
        [[[0, 9], 0]],
        [[[4, 5], 0]],
        [[[3, 0], 0]],
        [[[2, 6], 0]]
      ]
    end

    let(:player_positions) do
      [
        [[[1, 6], 0]],
        [[[1, 8], 0]],
        [[[8, 1], 0]],
        [[[9, 8], 0]],
        [[[0, 3], 0], [[1, 3], 0]],
        [[[0, 1], 0], [[1, 1], 0]],
        [[[4, 3], 0], [[4, 4], 0]],
        [[[3, 1], 0], [[4, 1], 0], [[5, 1], 0]],
        [[[4, 6], 0], [[4, 7], 0], [[4, 8], 0]],
        [[[7, 4], 0], [[7, 5], 0], [[7, 6], 0], [[7, 7], 0]]
      ]
    end

    before(:each) do
      allow(Positions).to receive(:load).with(Positions::PLAYER_TYPE[:player]).and_return(player_positions)
      allow(Positions).to receive(:load).with(Positions::PLAYER_TYPE[:computer]).and_return(computer_positions)

      allow(Positions).to receive(:save_ships)
    end

    it 'should check that response has status key' do
      post '/shoot', position: [1, 2]
      expect(JSON.parse(last_response.body).keys).to include('status')
    end

    it 'should check that response has cell key' do
      post '/shoot', position: [1, 2]
      expect(JSON.parse(last_response.body).keys).to include('cell')
    end

    it 'should check that response has opp_shoot key' do
      post '/shoot', position: [1, 2]
      expect(JSON.parse(last_response.body).keys).to include('opp_shoot')
    end

    context 'when the game can be continued' do
      it 'should check that status value equal to 0' do
        post '/shoot', position: [1, 2]
        expect(JSON.parse(last_response.body)['status']).to eq(0)
      end

      it 'should check that opp shoot has array' do
        post '/shoot', position: [1, 2]
        expect(JSON.parse(last_response.body)['opp_shoot']).to be_kind_of(Array)
      end

      context 'when the player hit' do
        it 'should check that cell value equal to true' do
          post '/shoot', position: [1, 2]
          expect(JSON.parse(last_response.body)['cell']).to be true
        end
      end
      context 'when the player missed' do
        it 'should check that cell value equal to false' do
          post '/shoot', position: [1, 9]
          expect(JSON.parse(last_response.body)['cell']).to be false
        end
      end
    end

    context 'when a player loses the game' do
      let(:player_positions) do
        [
          [[[1, 6], 1]],
          [[[1, 8], 1]],
          [[[8, 1], 1]],
          [[[9, 8], 1]],
          [[[0, 3], 1], [[1, 3], 1]],
          [[[0, 1], 1], [[1, 1], 1]],
          [[[4, 3], 1], [[4, 4], 1]],
          [[[3, 1], 1], [[4, 1], 1], [[5, 1], 1]],
          [[[4, 6], 1], [[4, 7], 1], [[4, 8], 1]],
          [[[7, 4], 1], [[7, 5], 1], [[7, 6], 1], [[7, 7], 0]]
        ]
      end

      it 'should check that status value equal to 2' do
        allow_any_instance_of(Opponent).to receive(:shoot).and_return([[7, 7], 1])

        post '/shoot', position: [9, 1]
        expect(JSON.parse(last_response.body)['status']).to eq(2)
      end
    end

    context 'when a player wins the game' do
      let(:computer_positions) do
        [
          [[[9, 1], 0], [[8, 1], 1], [[8, 0], 1], [[8, 2], 1]],
          [[[7, 4], 1], [[8, 4], 1], [[6, 4], 1]],
          [[[1, 2], 1], [[2, 2], 1], [[0, 2], 1]],
          [[[0, 6], 1], [[0, 7], 1]],
          [[[5, 1], 1], [[6, 1], 1]],
          [[[5, 9], 1], [[5, 8], 1]],
          [[[0, 9], 1]],
          [[[4, 5], 1]],
          [[[3, 0], 1]],
          [[[2, 6], 1]]
        ]
      end

      it 'should check that status value equal to 1' do
        post '/shoot', position: [9, 1]
        expect(JSON.parse(last_response.body)['status']).to eq(1)
      end
    end
  end
end
