require 'spec_helper'

describe Board do
  describe '#check_suitable' do
    let(:board) { Board.new }

    context 'when ship length more than 1' do
      before(:each) do
        allow_any_instance_of(Strategy).to receive(:initialize)
        allow_any_instance_of(Strategy).to receive(:places).and_return([[0, 2]])
      end

      context 'when ship is correct according strategy' do
        let(:ship) { [[0, 1], [0, 2]] }

        context 'when possible choices are available' do
          it 'should return true' do
            expect(board.check_suitable(ship)).to eql(true)
          end
        end
        context 'when possible choices are not available' do
          it 'should return false' do
            board.instance_variable_set(:@possible_choices, [])
            expect(board.check_suitable(ship)).to eql(false)
          end
        end
      end
      context 'when ship is not correct according strategy' do
        let(:ship) { [[0, 0], [9, 9]] }

        context 'when possible choices are available' do
          it 'should return false' do
            expect(board.check_suitable(ship)).to eql(false)
          end
        end
        context 'when possible choices are not available' do
          it 'should return false' do
            board.instance_variable_set(:@possible_choices, [])
            expect(board.check_suitable(ship)).to eql(false)
          end
        end
      end
    end
    context 'when ship length less than 2' do
      let(:ship) { [[0, 1]] }

      context 'when possible choices are available' do
        it 'should return true' do
          expect(board.check_suitable(ship)).to eql(true)
        end
      end
      context 'when possible choices are not available' do
        it 'should return false' do
          board.instance_variable_set(:@possible_choices, [])
          expect(board.check_suitable(ship)).to eql(false)
        end
      end
    end
  end

  describe '.clean_outer' do
    context 'when correct coords passed to the method' do
      it 'should return coords list' do
        expect(Board.clean_outer([[1, 9]])).to contain_exactly([1, 9])
      end
    end

    context 'when wrong coords passed to the method' do
      it 'should return false' do
        expect(Board.clean_outer([[1, 10]])).to be_empty
      end
    end
  end
end
