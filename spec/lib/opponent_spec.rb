require 'spec_helper'

describe Opponent do
  describe '#shoot' do
    let(:player_position) do
      [
        [[[1, 6], 0]],
        [[[1, 8], 0]],
        [[[2, 1], 0]],
        [[[9, 8], 0]],
        [[[0, 3], 0], [[1, 3], 0]],
        [[[0, 1], 0], [[1, 1], 0]],
        [[[4, 3], 0], [[4, 4], 0]],
        [[[3, 1], 0], [[4, 1], 0], [[5, 1], 0]],
        [[[4, 6], 0], [[4, 7], 0], [[4, 8], 0]],
        [[[0, 0], 0], [[0, 1], 0], [[0, 2], 0], [[0, 3], 0]]
      ]
    end

    let(:opponent) { Opponent.new(player_position) }

    before(:each) do
      allow_any_instance_of(Strategy).to receive(:places)
      # force the missing data file, get new empty data
      allow_any_instance_of(Opponent).to receive(:load)
      allow_any_instance_of(Opponent).to receive(:get_available_options).and_return(shoot_target)
    end

    context 'when shoot successful' do
      let(:shoot_target) { [[2, 1]] }

      context 'when opponent has not computed options' do
        before(:each) do
          allow(Board).to receive(:clean_outer).and_return([[2, 0], [2, 2], [1, 1], [3, 1]])
        end

        it 'should have 4 cells in computed options' do
          opponent.shoot
          expect(opponent.instance_variable_get(:@computed_options).size).to eql(4)
        end

        it 'should have 1 cell in positions' do
          opponent.shoot
          expect(opponent.instance_variable_get(:@positions).size).to eql(1)
        end
      end

      context 'when opponent has computed options' do
        context 'when opponent has positions with 3 decks' do
          let(:shoot_target) { [[0, 3]] }

          before(:each) do
            opponent.instance_variable_set(:@computed_options, [[1, 0], [1, 1], [1, 2], [0, 3]])
            opponent.instance_variable_set(:@positions, [[0, 0], [0, 1], [0, 2]])
          end

          it 'should reset computed options' do
            opponent.shoot
            expect(opponent.instance_variable_get(:@computed_options).size).to eql(0)
          end

          it 'should reset positions' do
            opponent.shoot
            expect(opponent.instance_variable_get(:@positions).size).to eql(0)
          end
        end

        context 'when opponent has not positions with 3 decks' do
          let(:shoot_target) { [[0, 1]] }

          before(:each) do
            allow(Board).to receive(:clean_outer).and_return([[0, 3]])
            opponent.instance_variable_set(:@computed_options, [[1, 0], [0, 1]])
            opponent.instance_variable_set(:@positions, [[0, 0]])
          end

          it 'should update positions' do
            opponent.shoot
            expect(opponent.instance_variable_get(:@positions).size).to eql(2)
          end

          it 'should update computed options' do
            opponent.shoot
            expect(opponent.instance_variable_get(:@computed_options).size).to eql(1)
          end
        end
      end
    end
  end
end
