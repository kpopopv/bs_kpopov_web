require 'spec_helper'

describe Strategy do
  describe '#places' do
    context 'when receive 1 coordinate' do
      let(:decks) { 2 }

      it 'return 4 cells' do
        strategy = Strategy.new([[4, 4]], decks)
        expect(strategy.places).to contain_exactly([4, 3], [4, 5], [3, 4], [5, 4])
      end
    end

    context 'when received 2 coordinate' do
      let(:decks) { 3 }

      context 'when horizontal orientation' do
        it 'return 2 cells' do
          strategy = Strategy.new([[4, 4], [4, 5]], decks)
          expect(strategy.places).to contain_exactly([4, 6], [4, 3])
        end
      end

      context 'when vertical orientation' do
        it 'return 2 cells' do
          strategy = Strategy.new([[4, 4], [5, 4]], decks)
          expect(strategy.places).to contain_exactly([3, 4], [6, 4])
        end
      end
    end

    context 'when received 3 coordinate' do
      let(:decks) { 4 }

      context 'when direct horizontal set' do
        it 'return 8 cells' do
          strategy = Strategy.new([[4, 4], [4, 5], [4, 6]], decks)
          expect(strategy.places).to contain_exactly([4, 3], [4, 7], [3, 4], [3, 5], [3, 6], [5, 4], [5, 5], [5, 6])
        end
      end

      context 'when direct vertical set' do
        it 'return 8 cells' do
          strategy = Strategy.new([[3, 4], [4, 4], [5, 4]], decks)
          expect(strategy.places).to contain_exactly([2, 4], [6, 4], [3, 3], [4, 3], [5, 3], [3, 5], [4, 5], [5, 5])
        end
      end

      context 'when top right set' do
        it 'return 4 cells' do
          strategy = Strategy.new([[3, 4], [4, 3], [4, 4]], decks)
          expect(strategy.places).to contain_exactly([2, 4], [4, 2], [4, 5], [5, 4])
        end
      end

      context 'when top left set' do
        it 'return 4 cells' do
          strategy = Strategy.new([[3, 3], [4, 3], [4, 4]], decks)
          expect(strategy.places).to contain_exactly([2, 3], [4, 5], [4, 2], [5, 3])
        end
      end

      context 'when bottom right set' do
        it 'return 4 cells' do
          strategy = Strategy.new([[4, 4], [4, 5], [5, 5]], decks)
          expect(strategy.places).to contain_exactly([4, 3], [6, 5], [3, 5], [4, 6])
        end
      end

      context 'when bottom left set' do
        it 'return 4 cells' do
          strategy = Strategy.new([[4, 4], [4, 5], [5, 4]], decks)
          expect(strategy.places).to contain_exactly([6, 4], [4, 6], [4, 3], [3, 4])
        end
      end
    end
  end
end
